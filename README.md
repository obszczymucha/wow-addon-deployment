# Deployment script for World of Warcraft addons

## Rationale
For some reason, when I `cd` to an addon's directory and run nvim, 
`coc` (or rather `lua-language-server`) processes and diagnoses files 
outside of the root directory. Having dozens of WoW addons makes the 
lua integration extremely clunky and slow, because it wants to process 
and analyze thousands of files.

## The idea
There are two directories:
  * "production": `/home/PrincessKenny/WoW/Interface/Addons`
  * "development": `/home/PrincessKenny/projects/lua/WoW`

Inside "development" directory there are projects under development, e.g.:
  * `/home/PrincessKenny/projects/lua/WoW/ModUi`
  * `/home/PrincessKenny/projects/lua/WoW/ModUiModulePack`

Each "development" addon directory has a structure:
  * `src/*`
  * anything else such as `.git/*` or whatever scripts

Inside "development" directory there must be a file called `.deploy-config`, 
which contains the path of the "production" directory.

## How it works

1. We run `./watch`.
2. The script reads the deployment directory from `.deploy-config`.
3. The script watches for changes recursively.
4. If any file is changed that matches `*/src/*` pattern, then the script 
   "deploys" that file to "production" by copying it.

